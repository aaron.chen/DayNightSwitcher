#!/usr/bin/bash

DAY_THEME="Vertex-Maia"
NIGHT_THEME="Vertex-Maia-Dark"

DAY_START=6
NIGHT_START=18

CHECK_INTERVAL=4

CHANGE_GTK2=true
CHANGE_GTK3=true

GTK2_CONFIG="${HOME}/.gtkrc-2.0"
GTK3_CONFIG="${HOME}/.config/gtk-3.0/settings.ini"

while :; do
	HOUR=$(date +%H)
	if [ "$HOUR" -gt "${NIGHT_START}" ] || [ "$HOUR" -lt "${DAY_START}" ]; then
		if [ CHANGE_GTK2 ]; then
			cp "${GTK2_CONFIG}" "${GTK2_CONFIG}.backup"
			sed -i -e "s/^gtk-theme-name=.*$/gtk-theme-name=\"${NIGHT_THEME}\"/" "${GTK2_CONFIG}"
		fi
		if [ CHANGE_GTK3 ]; then
			cp "${GTK3_CONFIG}" "${GTK3_CONFIG}.backup"
			sed -i -e "s/^gtk-theme-name=.*$/gtk-theme-name=${NIGHT_THEME}/" "${GTK3_CONFIG}"
		fi
	else
		if [ CHANGE_GTK2 ]; then
			cp "${GTK2_CONFIG}" "${GTK2_CONFIG}.backup"
			sed -i -e "s/^gtk-theme-name=.*$/gtk-theme-name=\"${DAY_THEME}\"/" "${GTK2_CONFIG}"
		fi
		if [ CHANGE_GTK3 ]; then
			cp "${GTK3_CONFIG}" "${GTK3_CONFIG}.backup"
			sed -i -e "s/^gtk-theme-name=.*$/gtk-theme-name=${DAY_THEME}/" "${GTK3_CONFIG}"
		fi
	fi

	sleep "${CHECK_INTERVAL}m"
done
