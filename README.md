# DayNightSwitcher

Simple script using sed to automatically change GTK theme from day theme to night theme (and back) depending on time of day.

Uses u/sandeepsb's NightMode.sh script at https://pastebin.com/addrzwA6 as a starting point.

Will refactor and document code a little better later.
